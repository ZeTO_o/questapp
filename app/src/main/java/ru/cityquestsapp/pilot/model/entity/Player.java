package ru.cityquestsapp.pilot.model.entity;

/**
 * Created by Roman Syrchin on 7/22/18.
 */
public interface Player {

    String getToken();
    String getName();
}
