package ru.cityquestsapp.pilot.model.entity;

/**
 * Created by Roman Syrchin on 7/22/18.
 */
public interface Quest {

    interface DbColumns {
        String ID = "ID";
        String NAME = "NAME";
        String AUTHOR = "AUTHOR";
        String LOCATION = "LOCATION";
        String DESCRIPTION = "DESCRIPTION";
    }

    String getTitle();
    String getDescription();

}
