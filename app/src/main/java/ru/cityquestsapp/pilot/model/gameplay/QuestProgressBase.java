package ru.cityquestsapp.pilot.model.gameplay;


import ru.cityquestsapp.pilot.model.entity.Player;
import ru.cityquestsapp.pilot.model.entity.Problem;
import ru.cityquestsapp.pilot.model.entity.Quest;
import ru.cityquestsapp.pilot.model.entity.Solution;

/**
 * Created by Roman Syrchin on 7/22/18.
 */
public abstract class QuestProgressBase {

    protected Player player;
    protected Quest quest;

    public Quest getQuest() { return quest; }
    public Player getPlayer() { return player; }

    abstract Problem startQuest();
    abstract Problem getNextProblem();
    abstract boolean trySolution(Problem problem, Solution solution);
    
}
